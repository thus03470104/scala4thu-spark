import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac019 on 2017/5/1.
  */
object HelloSpark extends App{

  val conf = new SparkConf().setAppName("HelloSpark")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

//  List[String]
  val nums: RDD[String] =sc.textFile("nums.txt")
//  nums.foreach(str=>{
//    println("========")
//    println(str)})
//  readLine()
  val range=1 to 100
  val intRdd: RDD[Int] =sc.parallelize(1 to 100)
  println("range:"+range)
  println("rdd:"+intRdd)

}
